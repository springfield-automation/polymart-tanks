import { Converter } from '@springfield/sensor';

const tankDepthEmptyTransducerReading = 1757;
const tankDepthFullTransducerReading = 300;
const tankVolume = 500;

export class Polymart500Converter implements Converter<number> {
  private numberTanks: number;

  constructor(numberTanks: number) {
    this.numberTanks = numberTanks;
  }

  convert(value: number): number {
    return this.numberTanks * tankVolume * (tankDepthEmptyTransducerReading - value) / (tankDepthEmptyTransducerReading - tankDepthFullTransducerReading);
  }
}
